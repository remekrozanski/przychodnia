/*
 * Copyright (c) 2019.
 * Remigiusz Rozanski
 * remekrozanski@wp.pl
 */

package pl.remigiusz.rozanski.przychodnia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.remigiusz.rozanski.przychodnia.model.Doctor;
import pl.remigiusz.rozanski.przychodnia.repository.DoctorRepository;

import java.util.Optional;

@Service
public class DoctorService {
    @Autowired
    private final DoctorRepository doctorRepository;

    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public Doctor save(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    public Iterable<Doctor> findAll() {
        return doctorRepository.findAll();
    }

    public Optional<Doctor> findById(Long doctorId) {
        return doctorRepository.findById(doctorId);
    }

    public void deleteDoctorById(Long doctorID) {
        doctorRepository.deleteDoctorById(doctorID);
    }

}