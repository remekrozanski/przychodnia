package pl.remigiusz.rozanski.przychodnia.repository;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.remigiusz.rozanski.przychodnia.model.Patient;
import java.util.Optional;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PatientRepositoryTest {

    Patient patient;

    @Mock
    PatientRepository repository;


    @Before
    public void setUp() throws Exception {
        patient = new Patient("test","test","test","test","test","test","test");

    }

    @Test
    public void savePatient() {
        Mockito.when(repository.save(patient)).thenReturn(patient);
        Patient result = repository.save(patient);
        assertEquals(patient,result);
    }

    @Test
    public void findPatientByID() {
        Optional<Patient> result = repository.findById(1l);   }
}