/*
 * Copyright (c) 2019.
 * Remigiusz Rozanski
 * remekrozanski@wp.pl
 */

package pl.remigiusz.rozanski.przychodnia.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import pl.remigiusz.rozanski.przychodnia.model.Doctor;

import java.util.Optional;


public interface DoctorRepository extends CrudRepository<Doctor, Long> {

    Optional<Doctor> findById(Long userId);

    @Transactional
    Long deleteDoctorById(Long doctorId);


}
