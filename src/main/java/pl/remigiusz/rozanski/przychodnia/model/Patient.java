package pl.remigiusz.rozanski.przychodnia.model;

import javax.persistence.*;
import java.util.HashSet;

import java.util.Set;


@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_patient;
    private String firstName;
    private String lastName;
    private String pesel;
    private String phone;
    private String address;
    private String fileReferences;
    private String medicines;
    @ManyToMany
    @JoinTable(
            name = "PATIENT_DOCTOR",
            joinColumns = {@JoinColumn(name = "ID_PATIENT")},
            inverseJoinColumns = {@JoinColumn(name = "ID")})

    private Set<Doctor> doctor;

    public Patient() {
    }

    public Patient(String firstName, String lastName, String pesel, String phone, String adress, String fileReferences, String medicines) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.phone = phone;
        this.address = adress;
        this.fileReferences = fileReferences;
        this.medicines = medicines;
        this.doctor = new HashSet<>();
    }

    public Long getId_patient() {
        return id_patient;
    }

    public void setId_patient(Long id_patient) {
        this.id_patient = id_patient;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFileReferences() {
        return fileReferences;
    }

    public void setFileReferences(String fileReferences) {
        this.fileReferences = fileReferences;
    }

    public String getMedicines() {
        return medicines;
    }

    public void setMedicines(String medicines) {
        this.medicines = medicines;
    }

    public void setDoctor(Set<Doctor> doctor) {
        this.doctor = doctor;
    }

    public Set<Doctor> getDoctor() {
        return doctor;
    }
}


