package pl.remigiusz.rozanski.przychodnia.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.remigiusz.rozanski.przychodnia.model.Patient;

import java.util.Optional;

@Repository
public interface PatientRepository extends CrudRepository<Patient,Long> {

    @Override
    Optional<Patient> findById(Long id);

    @Override
    Patient save(Patient patient);

    Optional<Patient> findByFirstNameAndLastName(String imnie,String nazwisko);
}
