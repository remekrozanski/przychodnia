/*
 * Copyright (c) 2019.
 * Remigiusz Rozanski
 * remekrozanski@wp.pl
 */

package pl.remigiusz.rozanski.przychodnia.model;

import javax.persistence.*;

@Entity
public class Room {
    @Id
    private short roomNumber;
    private String specialization;

    public Room() {
    }

    public Room(short roomNumber, String specialization) {
        this.roomNumber = roomNumber;
        this.specialization = specialization;
    }

    public short getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(short roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
