/*
 * Copyright (c) 2019.
 * Remigiusz Rozanski
 * remekrozanski@wp.pl
 */

package pl.remigiusz.rozanski.przychodnia;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrzychodniaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrzychodniaApplication.class, args);
    }

}
