package pl.remigiusz.rozanski.przychodnia.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class VisitDate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long visitDateId;
    private String receipts;
    private LocalDate date;
    private int timeVisit;
    @OneToOne
    @JoinColumn(name = "ID")
    private Doctor doctor;
    @OneToOne
    @JoinColumn(name = "ID_PATIENT")
    private Patient patient;
    @OneToOne
    @JoinColumn(name = "ROOM_NAME")
    private Room room;


    public Long getVisitDateId() {
        return visitDateId;
    }

    public void setVisitDateId(Long visitDateId) {
        this.visitDateId = visitDateId;
    }

    public String getReceipts() {
        return receipts;
    }

    public void setReceipts(String receipts) {
        this.receipts = receipts;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTimeVisit() {
        return timeVisit;
    }

    public void setTimeVisit(int timeVisit) {
        this.timeVisit = timeVisit;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
