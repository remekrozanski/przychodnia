/*
 * Copyright (c) 2019.
 * Remigiusz Rozanski
 * remekrozanski@wp.pl
 */

package pl.remigiusz.rozanski.przychodnia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.remigiusz.rozanski.przychodnia.model.Doctor;
import pl.remigiusz.rozanski.przychodnia.service.DoctorService;


@RestController
@RequestMapping(path = "/doctors")
public class DoctorController {

    private final DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping
    public @ResponseBody
    Iterable<Doctor> getAllDoctors() {
        return doctorService.findAll();
    }

    @PostMapping
    public Doctor addNewDoctor(@RequestBody Doctor doctor) {
        return doctorService.save(doctor);
    }

    @GetMapping("/{doctorId}")
    public ResponseEntity findDoctorById(@PathVariable Long doctorId) {

        return ResponseEntity.of(doctorService.findById(doctorId));

    }

    @DeleteMapping(value = "/delete/{doctorId}")
    public @ResponseBody
    void deleteDoctorById(@PathVariable Long doctorId) {
        doctorService.deleteDoctorById(doctorId);
    }

}
