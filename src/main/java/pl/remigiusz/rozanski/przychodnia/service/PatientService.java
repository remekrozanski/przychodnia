package pl.remigiusz.rozanski.przychodnia.service;

import pl.remigiusz.rozanski.przychodnia.model.Patient;

import java.util.Optional;

public interface PatientService  {

    Optional<Patient> findByID(Long id);

    void savePatient(Patient patient);


}
